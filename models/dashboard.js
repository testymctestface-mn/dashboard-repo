'use strict';
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const WIDGET = {
  canvas: String,
  id: String,
  x: Number,
  y: Number,
  height: Number,
  width: Number,
  type: { type: String },
  // Necessary since `type' is a reserved word in Mongoose
  inputs: Schema.Types.Mixed,
  innerWidgets: Schema.Types.Mixed,
  percentage: Number,
  order: Number,
  valid: {
    type: Number,
    default: 1,
  },
};

const VARIABLE = {
  name: String,
  class: String,
  device: String,
};

let dashboardSchema = mongoose.Schema({
  _id: {
    type: ObjectId,
    auto: true,
  },
  name: {
    type: String,
    required: true,
  },
  user: {
    type: String,
    required: true,
  },
  environment: {
    type: [String],
    default: ['*/*'],
    required: true,
  },
  widgets: {
    type: [WIDGET],
    required: true,
  },
  variables: {
    type: [VARIABLE],
    required: false,
    default: [],
  },
  insertTime: {
    type: Date,
    default: Date.now,
  },
  updateTime: {
    type: Date,
    required: true,
    default: Date.now,
  },
  group: {
    type: String,
    required: false,
    default: null,
  },
  groupWriteAccess: {
    type: Boolean,
    required: false,
    default: false,
  },
  lastUpdatedBy: {
    type: String,
    required: false,
    default: null,
  },
  deleted: {
    type: Boolean,
    default: false,
  },
  tangoDB: {
    type: String,
    default: '',
  },
});

dashboardSchema.statics.isNameUsed = async function(
  name,
  user,
  tangodb,
  environment,
) {
  let foundFlag = false;
  try {
    // Convert environment to a regex array to match any of the environments.
    const environmentRegexArray = environment.map(
      env => new RegExp(`^${env}$`),
    );
    const dashboard = await
    this.findOne({
      name: name,
      deleted: { $ne: true },
      user: user, tangodb: tangodb,
      environment: { $in: environmentRegexArray },
    });
    if (dashboard)
      foundFlag = true;
  } catch (e) {
    console.log('Error in dashboardSchema.statics.isNameUsed: ', e.message);
  }
  return foundFlag;
};

module.exports = mongoose.model('Dashboard', dashboardSchema);
