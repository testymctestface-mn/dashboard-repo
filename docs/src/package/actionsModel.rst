**************************************************
Data model for storing the user action log details
**************************************************

.. code-block:: javascript
  :linenos:

  let userActionLog = mongoose.Schema({
    actionType: {
        type: String,
        required: true,
    },
    timestamp: {
        type: Date,
        required: true,
        default: Date.now,
    },
    user: {
        type: String,
        required: true,
    },
    tangoDB: {
        type: String,
        default: '',
    },
    device: {
        type: String,
        required: true,
    },
    name: {
        type: String,
    },
    value: {
        type: Schema.Types.Mixed,
    },
    argin: {
        type: String,
    },
    valueBefore: {
        type: Schema.Types.Mixed,
    },
    valueAfter: {
        type: Schema.Types.Mixed,
    },
  });