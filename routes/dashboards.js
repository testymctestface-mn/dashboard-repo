'use strict';
/**
 * Handles urls prefixed with /dashboards
 */
const express = require('express');
const jsonwebtoken = require('jsonwebtoken');
const mongoose = require('mongoose');
const router = express.Router();
var Dashboard = require('../models/dashboard');
const config = require('../config.json');

/**
 * Returns a count of shared dashboards
 * for each group the current user belongs to, for a specific tangodb.
 */
router.get('/group/dashboardsCount', async(req, res, next) => {
  const token = req.cookies.taranta_jwt;
  const excludeCurrentUser = req.query.excludeCurrentUser === 'true';
  const tangoDB = req.query.tangoDB || '';
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error){
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const data = {};
  for (let i = 0; i < groups.length; i++){
    try {
      // allowing missing tango db is really just for backwards compatibility.
      // Can be removed when all dashboard has this set
      const query = {
        group: groups[i],
        deleted: { $ne: true },
        tangoDB: {$in: ['', tangoDB, null]},
      };
      if (excludeCurrentUser){
        query['user'] = {$ne: username};
      }
      data[groups[i]] = await Dashboard.countDocuments(query);
    } catch (error){
      console.log(error);
    }
  }
  res.send(data);
});
/**
 * Return a list of dashboard that belongs to a specific group
 * for a specific tangodb.
 * Does not return the widgets
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * group: AD group this dashboard is shared with, or null
 */
router.get('/group/dashboards', function(req, res, next) {
  const token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const requestedGroup = req.query.group;
  const tangoDB = req.query.tangoDB || '';
  const excludeCurrentUser = req.query.excludeCurrentUser === 'true';
  if (!requestedGroup){
    res.status(400).send('Missing query parameter group');
    return;
  }
  if (!groups.includes(requestedGroup)) {
    res.sendStatus(403);
    return;
  }
  const query = {
    group: requestedGroup,
    deleted: { $ne: true },
    tangoDB: {$in: ['', tangoDB, null]},
  };
  if (excludeCurrentUser){
    query['user'] = {$ne: username};
  }
  Dashboard.find(query, function(
    err,
    result,
  ) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        var list = [];
        result.forEach(function(result) {
          const {
            name,
            user,
            insertTime,
            updateTime,
            group,
            groupWriteAccess,
            lastUpdatedBy,
            tangoDB,
            variables,
          } = result;
          list.push({
            id: result._id,
            name,
            user,
            insertTime,
            updateTime,
            group,
            groupWriteAccess,
            lastUpdatedBy,
            tangoDB,
            variables,
          });
        });
        res.send(list);
      }
    }
  });
});

function addUsernameFilter(filter, username) {
  if (config.ADMIN_USER !== username)
    filter.user = username;
}

function addEnvFilter(filter, username, environmentQuery) {
  // For admin user show dashboard from all users
  if (config.ADMIN_USER === username)
    return;

  // Create a regular expression for the environment filter
  let environmentRegex = environmentQuery.map(env => {
    if (env === '*/*') {
      return new RegExp('.*'); // Matches everything
    } else {
      // Replace '*' with '.*' for regex wildcard
      let pattern = env.replace(/\*/g, '.*');
      // Exact match with start and end
      return new RegExp(`^${pattern}$`);
    }
  });

  filter['$or'] = [
    { environment: { $in: environmentRegex } },
    { environment: '*/*' },
    { environment: null },
    { environment: [] },
    { environment: { $exists: false } },
    // Include old dashboards without the environment key
  ];
}

/**
 * Return a list of dashboard that belongs to a specific user
 * for selected environments
 * for a specific tangodb.
 * Does not return the widgets
 * returns
 * id:string
 * name: string
 * user: string
 * environment: [string]
 * insertTime: Date
 * updateTime: Date
 * group: AD group this dashboard is shared with, or null
 */
router.get('/user/dashboards', function(req, res, next) {
  const token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    console.log('decodeToken error: ', error);
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log(
    'Get called for username : ',
    username,
    ' group: ',
    groups,
  );

  const tangoDB = req.query.tangoDB || '';
  const environmentQuery = req.query.environment || [];
  console.log('tangoDB: ', tangoDB, 'environmentQuery: ', environmentQuery);

  const filter = {
    deleted: { $ne: true },
    tangoDB: { $in: ['', tangoDB, null] },
  };

  addUsernameFilter(filter, username);
  addEnvFilter(filter, username, environmentQuery);
  Dashboard.find(filter,
    function(err, results) {
      if (err) {
        res.sendStatus(404);
      } else {
        if (results) {
          var list = [];
          results.forEach(function(result) {
            const {
              _id,
              name,
              environment,
              user,
              insertTime,
              updateTime,
              group,
              lastUpdatedBy,
              tangoDB,
              variables,
            } = result;

            const dashWithSameName = results.filter(
              dash => (_id !== dash._id && name === dash.name),
            );
            if (dashWithSameName && dashWithSameName.length > 0) {
              dashWithSameName.forEach((dash, i) => {
                i = i + 1;
                const newName = (dash.name + ' - copy ' + i);
                updateDash(dash._id, newName);

                results.forEach(function(recc) {
                  if (recc._id === dash._id) {
                    recc.name = newName;
                  }
                });
              });
            }

            list.push({
              id: result._id,
              name,
              user,
              environment,
              insertTime,
              updateTime,
              group,
              lastUpdatedBy,
              tangoDB,
              variables,
            });
          });
          res.send(list);
        }
      }
    },
  );
});

async function updateDash(id, newName) {
  try {
    await Dashboard.findOneAndUpdate(
      {
        _id: new mongoose.mongo.ObjectID(id),
        deleted: { $ne: true },
      },
      { $set: { name: newName } },
    );
  } catch (e) {
    console.log('Error while updating dashboard: ', e.message);
  }
}

/**
 * Return a full dashboard by id.
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * widgets: any[]
 */
router.get('/:id', function(req, res, next) {
  var id = req.params.id;
  Dashboard.findOne({ _id: id, deleted: { $ne: true } }, function(err, result) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        console.log('Serving dashboard: ' + result);
        const {
          name,
          user,
          environment,
          variables,
          insertTime,
          updateTime,
          group,
          groupWriteAccess,
          lastUpdatedBy,
          tangoDB,
        } = result;
        res.send({
          id: result._id,
          name,
          environment,
          user,
          insertTime,
          updateTime,
          widgets: result.widgets,
          variables,
          group,
          groupWriteAccess,
          lastUpdatedBy,
          tangoDB,
        });
      } else {
        res.sendStatus(404);
      }
    }
  });
});
/**
 * Delete a dashboard that belongs to the user by id
 * returns
 * id: string
 * deleted: boolean
 */
router.delete('/:id', function(req, res, next) {
  var id = req.params.id;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log(
    'Delete called for username : ',
    username,
    ' group: ',
    groups,
    ' id : ',
    id,
  );
  const filter = { _id: id, deleted: { $ne: true } };
  addUsernameFilter(filter, username);

  Dashboard.findOneAndUpdate(
    filter,
    { $set: { deleted: true } },
    { new: true },
    function(err, result) {
      if (err) {
        res.send({ id, deleted: false });
      } else {
        if (result) {
          res.send({ id: result._id, deleted: true });
        } else {
          res.sendStatus(403);
        }
      }
    },
  );
});
/**
 * Share a dashboard to an AD group
 */
router.post('/:id/share', function(req, res) {
  var id = req.params.id;
  var token = req.cookies.taranta_jwt;
  var group = req.body.group;
  var groupWriteAccess = req.body.groupWriteAccess || false;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log('Sharing dashboard ', id, ' groups ', groups);
  Dashboard.findById(id, function(err, dashboard) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    }
    if (!dashboard) {
      res.sendStatus(404);
      return;
    }
    if (dashboard.user !== username) {
      res.sendStatus(403);
      return;
    }
    dashboard.group = group;
    dashboard.groupWriteAccess = groupWriteAccess;
    dashboard.save(function(err) {
      if (err) {
        res.sendStatus(500);
        console.log(err);
      } else {
        res.send({ id: dashboard._id + '', created: false });
      }
    });
  });
});
/**
 * Clone a dashboard using id
 */
router.post('/:id/clone', function(req, res, next) {
  var id = req.params.id;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log('Clone dashboard  username: ', username, ' groups ', groups);
  Dashboard.findOne({ _id: id, deleted: { $ne: true } }, function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(404);
    } else {
      if (result) {
        insertDashboard(
          result.name + ' - copy',
          username,
          result.environment,
          result.widgets,
          result.variables,
          result.tangoDB,
          res);
      } else {
        res.sendStatus(404);
      }
    }
  });
});
/**
 * Rename a dashboard using id
 * returns id
 */
router.post('/:id/rename', async function(req, res, next) {
  var id = req.params.id;
  var tangoDB = req.params.tangoDB;
  var new_name = req.body.newName;
  var environment = req.body.environment;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log('Rename dashboard for username: ',
    username, ' groups ', groups, 'environment', environment);

  // Check if dashboard already exists with new name
  const dash = await Dashboard.isNameUsed(
    new_name,
    username,
    tangoDB,
    environment,
  );
  if (!dash) {
    Dashboard.findOneAndUpdate(
      {
        _id: new mongoose.mongo.ObjectID(id),
        user: username,
        deleted: { $ne: true },
      },
      { $set: { name: new_name } },
      { new: true },
      function(err, result) {
        if (err) {
          res.send(404);
        } else {
          if (result) {
            res.send({ id: result._id });
          } else {
            res.sendStatus(404);
          }
        }
      },
    );
  } else {
    res.status(403).json(
      {error: `Dashboard with name ${new_name} already exists`},
    );
  }
});

/**
 * Updates specified fields on dashboard
 *
 */
router.post('/:id/update', async function(req, res, next) {
  var id = req.params.id;
  var fields = req.body.fields;
  var token = req.cookies.taranta_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username } = decoded;

  // Allowed field to update on dashboard
  const validFields = ['environment'];
  const invalidFields =
  Object.keys(fields).filter(x => !validFields.includes(x));

  const filter = {
    _id: new mongoose.mongo.ObjectID(id),
    deleted: { $ne: true },
  };
  addUsernameFilter(filter, username);

  if (invalidFields.length === 0) {
    Dashboard.findOneAndUpdate(
      filter,
      { $set: fields },
      { new: true },
      function(err, result) {
        if (err) {
          res.send(404);
        } else {
          if (result) {
            res.send({ id: result._id });
          } else {
            res.sendStatus(404);
          }
        }
      },
    );
  } else {
    res.status(403).json(
      {error: `Update not allowed for Dashboard fields:
      ${invalidFields.toString()}.`},
    );
  }
});

/**
 * Creates or updates a dashboard.
 * returns
 * id:string - the id of the dashboard that was updated or created
 * created:boolean - true if the dashboard was created
 */
router.post('/', function(req, res, next) {
  var body = req.body;
  var id = body.id;
  var name = body.name;
  var environment = body.environment || ['*/*'];
  var widgets = body.widgets;
  var tangoDB = body.tangoDB || '';
  var token = req.cookies.taranta_jwt;
  var variables = body.variables;
  const { username, groups } = decodeToken(token);
  console.log('Variables: ', variables);

  if (id && id !== '') {
    try {
      updateDashboard(id, username, environment, groups,
        widgets, variables, tangoDB, res);
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  } else {
    try {
      insertDashboard(name, username,
        environment, widgets, variables, tangoDB, res);
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  }
});

const updateDashboard = (id, username, environment, groups,
  widgets, variables, tangoDB, res) => {
  console.log('Updating dashboard ' + id);
  Dashboard.findById(id, function(err, dashboard) {
    if (err) {
      console.log(err);
      res.status(500).send({error: 'Failed to updated dashboard'});
      return;
    }
    if (!dashboard) {
      res.status(404).json({error: 'Dashboard not found'});
      return;
    }
    if (config.ADMIN_USER !== username) {
      const mine = dashboard.user === username;
      const sharedWithMe = dashboard.group &&
        groups.includes(dashboard.group) &&
        dashboard.groupWriteAccess;
      if (!(mine || sharedWithMe)) {
        res.status(403).json(
          {error: 'Access denied to update dashboard: ' + dashboard.name},
        );
        return;
      }
    }
    dashboard.environment = environment || ['*/*'];
    dashboard.widgets = widgets;
    dashboard.variables = variables;
    dashboard.updateTime = new Date();
    dashboard.lastUpdatedBy = username;
    dashboard.tangoDB = tangoDB;
    dashboard.markModified('widgets');
    dashboard.save(function(err) {
      if (err) {
        res.status(500).json(
          {error: 'Failed to update dashboard: ' + dashboard.name},
        );
        console.log(err);
      } else {
        res.send({ id: dashboard._id + '', created: false });
      }
    });
  });
};
const insertDashboard = async(
  name, username, environment, widgets, variables, tangoDB, res,
) => {
  const dash = await Dashboard.isNameUsed(name, username, tangoDB, environment);
  if (!dash) {
    var dashboard = new Dashboard();
    console.log('Inserting dashboard ' + dashboard._id);
    dashboard.widgets = widgets;
    dashboard.variables = variables;
    dashboard.user = username;
    dashboard.environment = environment;
    dashboard.name = name || 'New dashboard';
    dashboard.insertTime = new Date();
    dashboard.lastUpdatedBy = username;
    dashboard.tangoDB = tangoDB;
    dashboard.save(function(err) {
      if (err) {
        res.sendStatus(500);
        console.log('ERROR?');
        console.dir(err);
      } else {
        res.send({ id: dashboard._id + '', created: true });
      }
    });
  } else {
    res.status(403).json({error: `Dashboard with name ${name} already exists`});
  }
};

const decodeToken = token => {
  try {
    const decoded = jsonwebtoken.verify(token, process.env.SECRET);
    return decoded;
  } catch (exception) {
    throw Error('jwt authorization failed: ' + exception);
  }
};
exports.decodeToken = decodeToken;
exports.router = router;
