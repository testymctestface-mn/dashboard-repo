'use strict';

/**
 * Handles urls prefixed with /elasticsearch
 */
const express = require('express');
const router = express.Router();
const https = require('https');
const fs = require('fs');
const axios = require('axios').default;
const config = require('../config.json');
const elasticPath = '/../secrets/elasticsearch/';
const encoding = 'utf8';

// retrieve information to connect to ElasticCluster
let caCertificate = '';
try {
  caCertificate = fs.readFileSync(
    __dirname + elasticPath + config.elastic_caCertificate, encoding,
  );
} catch (err) {
  if (err.code === 'ENOENT') {
    console.error('File not found:', err.path);
  } else {
    console.error('Error reading file:', err);
  }
}

let clientCertificate = '';
try {
  clientCertificate = fs.readFileSync(
    __dirname + elasticPath + config.elastic_clientCertificate, encoding,
  );
} catch (err) {
  if (err.code === 'ENOENT') {
    console.error('File not found:', err.path);
  } else {
    console.error('Error reading file:', err);
  }
}

let clientKey = '';
try {
  clientKey = fs.readFileSync(
    __dirname + elasticPath + config.elastic_clientKey, encoding,
  );
} catch (err) {
  if (err.code === 'ENOENT') {
    console.error('File not found:', err.path);
  } else {
    console.error('Error reading file:', err);
  }
}

let apiKey = '';
try {
  apiKey = fs.readFileSync(
    __dirname + elasticPath + config.elastic_apiKey, encoding,
  );
} catch (err) {
  if (err.code === 'ENOENT') {
    console.error('File not found:', err.path);
  } else {
    console.error('Error reading file:', err);
  }
}

const apiInstance = axios.create({
  baseURL: config.elastic_baseUrl,
  httpsAgent: new https.Agent({
    ca: caCertificate,
    cert: clientCertificate,
    key: clientKey,
  }),
  headers: {
    Authorization: 'ApiKey ' + apiKey, // + config.elastic_apiKey
  },
});

/**
 * Test if Elasticsearch Cluster is alive
 * returns response: json
 */
router.get('/health', async function(req, res, next) {

  console.log('Elasticsearch health query');
  try {
    const response = await apiInstance.get('/_cat/health');
    res.send(response.data);
  } catch (error) {
    console.error('Error fetching data:', error);
    res.status(500).send(JSON.stringify({error: 'Error fetching data'}));
  }
});

/**
 * Perform Elasticsearch query usign the ApiKey
 * returns response: json
 */
router.post('/_search', async function(req, res, next) {
  console.log('Elasticsearch query');
  try {
    const response = await apiInstance.post('/_search',
      req.body,
    );
    res.send(response.data);
  } catch (error) {
    console.error('Error fetching data:', error);
    res.status(500).send(JSON.stringify({error: 'Error fetching data'}));
  }
});

exports.router = router;
